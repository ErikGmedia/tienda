<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>GB MEDIA</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css" />

    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="../css/slick.css" />
    <link type="text/css" rel="stylesheet" href="../css/slick-theme.css" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="../css/nouislider.min.css" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="../css/style.css" />

    <link rel="icon" type="image/png" href="../img/elIco.ico">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->


    <!--======================================================================================================-->
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->

    <link rel="stylesheet" type="text/css" href="../css/gsdk-bootstrap-wizard.css">
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/carrito.css">
    <!--================================================================
 																PRELOADER
		====================================================================-->

    <link type="text/css" rel="stylesheet" href="../css/loader.css" />
    <!--
				PRELOADER SCRIPTS
			-->
    <script src="../js/pace.min.js"></script>
    <script src="../js/modernizr.js"></script>

</head>

<body id="top">
    <!-- HEADER -->
    <header>
        <!-- MAIN HEADER -->
        <div id="header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- LOGO -->
                    <div class="col-md-3">
                        <div class="header-logo">
                            <a href="../index.php" class="logo">
                                <img src="../img/logoGBMediaGroup2.png" alt="Logo de grupo bedoya">
                            </a>
                        </div>
                    </div>
                    <!-- /LOGO -->

                    <!-- SEARCH BAR -->
                    <div class="col-md-6">
                    </div>
                    <!-- /SEARCH BAR -->

                    <!-- ACCOUNT -->
                    <div class="col-md-3 clearfix">
                        <div class="header-ctn">
                            <div class="wrap-icon-header flex-w flex-r-m h-full">
                                <div class="flex-c-m h-full p-r-25 bor6">
                                    <div class="icon-header-item cl0 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" id="detail_cart" data-notify="0" style="">
                                        <i class='zmdi zmdi-shopping-cart' id="carrito"></i>
                                    </div>
                                </div>
                            </div>


                            <div id="show_carrito" class="wrap-header-cart js-panel-cart ">
                                <div class="s-full js-hide-cart"></div>

                                <div class="header-cart flex-col-l p-l-65 p-r-25">
                                    <div class="header-cart-title flex-w flex-sb-m p-b-8">
                                        <span class="mtext-103 cl2">
													Carrito
												</span>

                                        <div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
                                            <i id='close_carrito' class="zmdi zmdi-close"></i>
                                        </div>
                                    </div>

                                    <div class="header-cart-content flex-w js-pscroll">
                                        <ul class="header-cart-wrapitem w-full barra_carrito">

                                        </ul>

                                        <div class="w-full">
                                            <div class="header-cart-total w-full p-tb-40" id="total_cart">

                                            </div>

                                            <div class="header-cart-buttons flex-w w-full">
                                                <a href="car.html" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
															Ver Carrito
														</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Cart -->
                        </div>
                    </div>
                    <!-- /ACCOUNT -->
                </div>
                <!-- row -->
            </div>
            <!-- container -->
        </div>
        <!-- /MAIN HEADER -->
    </header>
    <!-- /HEADER -->

    <section id="cart_items">
        <div class="">
            <!--   Big container   -->
            <div class="container">
                <div class="">
                    <div class="col-md-12">
                        <!--      Wizard container        -->
                        <div class="wizard-container">
                            <div class="card wizard-card" data-color="azzure" id="wizard">
                                <form method="POST" id="form-comprar">
                                    <!--        You can switch ' data-color="azzure" '  with one of the next bright colors: "blue", "green", "orange", "red"          -->

                                    <div class="wizard-header">
                                        <h3>
                                            <b>LISTA DE COMPRA  </b><br>
                                            <small>Esta información nos permitirá saber más sobre usted y los productos que desea.</small>
                                        </h3>
                                    </div>
                                    <div class="wizard-navigation">
                                        <ul>
                                            <li><a class="aCarrito" href="#details" data-toggle="tab">Detalle del carrito</a></li>
                                            <li><a class="aCarrito" href="#captain" data-toggle="tab">Información Personal</a></li>
                                        </ul>
                                    </div>
                                    <div class="tab-content" id="detail_user">
                                        <div class="tab-pane" id="details">
                                            <div class="col-md-12" id="content_product">

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="captain">
                                            <h4 class="info-text">Información Personal</h4>
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="full_name">Nombre completo <small>(required)</small></label>
                                                        <input type="text" class="form-control" id="full_name" placeholder="Nombre completo" name="full_name" required value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="phone_contact">Telefono de contacto <small>(required)</small></label>
                                                        <input type="text" class="form-control" id="phone_contact" placeholder="Telefono de contacto" name="phone_contact" required value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email">Email <small>(required)</small></label>
                                                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" required value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="location">Locación <small>(required)</small></label>
                                                        <input type="text" class="form-control" id="location" placeholder="Cali, Valle del cauca" name="location" required value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="address">Dirección <small>(required)</small></label>
                                                        <input type="text" class="form-control" id="address" placeholder="Dirección" name="address" required value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="description">
                                            <div class="col-md-12" id="content_product_detail">

                                            </div>
                                            <input type="hidden" name="bought" id="bought" value="">
                                            <input type="hidden" name="locacion" id="locacion" value="">
                                            <input type="hidden" name="estudio" id="estudio" value="">
                                            <input type="hidden" name="rol" id="rol" value="">
                                        </div>
                                    </div>

                                    <div class="wizard-footer">
                                        <div class="pull-right">
                                            <input type='button' class='btn btn-next btn-fill btn-info btn-wd btn-sm' name='next' value='Siguiente' />
                                            <input type='button' class='btn btn-finish btn-fill btn-info btn-wd btn-sm' name='finish' id="comprar" value='Comprar!' />
                                        </div>
                                        <div class="pull-left">
                                            <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Volver' />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- wizard container -->
                    </div>
                </div>
                <!-- row -->
            </div>
            <!--  big container -->
        </div>

        <!-- FOOTER -->
        <footer id="footer">
            <!-- top footer -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                                <div class="footer">
                                    <h3 class="footer-title" >Gb MEDIA GROUP</h3>
                                    <ul class="footer-links">
                                        <p class="stext-107 cl7 " >
                                            Somos un grupo empresarial joven y especializado en la creación y desarrollo de negocios online, obteniendo resultados que satisfacen sus necesidades.
                                        </p>
                                    </ul>
                                </div>
                        </div>

                        <div class="col-md-3 col-xs-3">
                            <div class="footer"  style="visibility:hidden;">
                                <h3 class="footer-title">Categorias</h3>
                                <ul class="footer-links">
                                    <li><a href="#">Lenceria</a></li>
                                    <li><a href="#">Lubricantes</a></li>
                                    <li><a href="#">Disfraces</a></li>
                                    <li><a href="#">Condones</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="clearfix visible-xs"></div>

                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- /top footer -->

        </footer>
        <!-- /FOOTER -->



        <!--===============================================================================================================-->

        <!-- Back to top -->
        <div class="btn-back-to-top" id="myBtn">
            <span class="symbol-btn-back-to-top">
			<i class="zmdi zmdi-chevron-up"></i>
		</span>
        </div>
        <script>
          /*  $(".js-select2").each(function() {
                $(this).select2({
                    minimumResultsForSearch: 20,
                    dropdownParent: $(this).next('.dropDownSelect2')
                });
            })*/
        </script>
        <script>
           /* $('.js-pscroll').each(function() {
                $(this).css('position', 'relative');
                $(this).css('overflow', 'hidden');
                var ps = new PerfectScrollbar(this, {
                    wheelSpeed: 1,
                    scrollingThreshold: 1000,
                    wheelPropagation: false,
                });

                $(window).on('resize', function() {
                    ps.update();
                })
            });*/
        </script>
        <!--===============================================================================================-->
        <!-- jQuery Plugins -->

    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/jquery.validate.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/slick.min.js"></script>
    <script src="../js/nouislider.min.js"></script>
    <script src="../js/loader.js"></script>
    <script src="../js/jquery.bootstrap.wizard.js"></script>
    <script src="../js/gsdk-bootstrap-wizard.js"></script>
    <script src="../js/main.js"></script>
    <script src="../js/device-uuid.js"></script>
    <script src="../js/device-uuid.min.js"></script>
    <script src="../js/cart.js"></script>

        <script>

            $(document).ready(function() {

                var validar_formulario = $("#form-comprar").validate({

                    rules: {
                        full_name: { required: true},
                        phone_contact: { required: true},
                        email: { required: true},
                        location: { required: true},
                        address: { required: true}
                    },

                    messages: {
                        full_name: "Este campo es requerido",
                        phone_contact: "Este campo es requerido",
                        email: "Este campo es requerido",
                        location: "Este campos es requerido",
                        address: "Este campo es requerido"
                    },

                });

                $("#comprar").click(function(e) {

                    if(validar_formulario.form()){
                        $.ajax({
                            type: 'POST',
                            url: 'http://localhost/gb/gb/ajaxComprar',
                            data: $('#form-comprar').serialize(),
                            dataType : 'json',
                            success: function(data){
                                //console.log(data);
                                clearCart();
                                window.location.href="../index.php?m=ok";
                            },
                            error: function(data){
                                console.log("Error");
                            }
                        });
                    }
                });

            })
        </script>

</body>

</html>