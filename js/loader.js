$(document).ready(function() {

    //reload all products
    $(document).on("click","#homeNav",function (){
        $("#tab1").addClass("getUpList");                
        $("#tab1").css("display","block");
        $("#list_product").css("display","none");
    });
    
    
    //force page scroll position to top at page refresh
     $('html, body').animate({ scrollTop: 0 }, 'normal');

    // will first fade out the loading animation 
    $("#loader").fadeOut("slow", function() {
        // will fade out the whole DIV that covers the website.
        $("#preloader").delay(300).fadeOut("slow");
    });

    // for hero content animations 
    $("html").removeClass('cl-preload');
    $("html").addClass('cl-loaded');


  /*
   *                                    STICKY MENU 
   * 
   * 
   */
    var altura=$("#navigation").offset().top;
    
    $(window).on("scroll",function(){
        if($(window).scrollTop()>altura){
            $("#navigation").addClass("menu-fixed");
        }
        else{
            $("#navigation").removeClass("menu-fixed");
        }
    });
    
    $(document).on("click",".Scroller",function(e){
        var screen=$(window).width();
        e.preventDefault();
        if($(window).width()>1400){
            $('html, body').animate({ scrollTop: 1400 }, 'slow');    
        }
        else if($(window).width()<1400 && $(window).width()>=730){
            if(screen>=731 && screen<=823){
                if($(window).width()==411 && $(window).height()==812){$('html, body').animate({ scrollTop: 2500 }, 'slow');}
                else if($(window).width()==411 && $(window).height()==812){$('html, body').animate({ scrollTop: 2500 }, 'slow');}
                else if($(window).width()<=823 && $(window).height()<=414){$('html, body').animate({ scrollTop: 1800 }, 'slow');}
                else if($(window).width()<=768 && $(window).height()<=1024){$('html, body').animate({ scrollTop: 1800 }, 'slow');}
            }
            else{
               if($(window).width()<=1024 && $(window).height()<=1366){console.log("jiji");$('html, body').animate({ scrollTop: 2600 }, 'slow');}
               else{$('html, body').animate({ scrollTop: 1100 }, 'slow');}
            }
        }
        else if($(window).width()<730){            
            if(screen<=360 || screen==375){
                $('html, body').animate({ scrollTop: 1550 }, 'slow');     
            }
            else if(screen>=360 && screen<=667){
                if(screen==411 || screen==414){$('html, body').animate({ scrollTop: 1450 }, 'slow');}
                else if(screen>=568 && screen<=660){$('html, body').animate({ scrollTop: 1750 }, 'slow');}
                else if(screen>660 && screen<=770){$('html, body').animate({ scrollTop: 1750 }, 'slow');}
                else{$('html, body').animate({ scrollTop: 2500 }, 'slow');}                    
            }               
        }
        if($("#responsive-nav").attr("class")=="active"){
            $(".menu-toggle > a").click();
        }
        
    });
    
    /*
    ===================================================================================================
                                            get up
    ===================================================================================================
    */

    var iScrollPos=0;

    $(window).scroll(function(){
        var iCurScrollPos = $(this).scrollTop();
        if(iCurScrollPos>iScrollPos){      
            if($(window).width()>=1024){
                $(".section").addClass("getUp");    
            }                           
       }
    });

    $(".item .active",function(){
        $(".btnCarousel").addClass("getUpBtn");
    });

    /*
    ==========================================================================================================
                                            sectionProd
    ==========================================================================================================
    */

    var iScrollPos_2=0;
    var cont=0;
    var indice=1286;
    $("#tab1").scroll(function(){
       
       var iCurScrollPos = $(this).scrollTop();
       if(iCurScrollPos>iScrollPos_2){    
           try{            
                $("#"+$(".divHidden")[cont].id).removeClass("divHidden");
       
                $("#"+$(".divHidden")[cont].id).addClass("getUpList");
           } 
           catch{

           }      
        }
       else {
           try{
            $("#"+$(".divHidden")[cont].id).addClass("divHidden");

            $("#"+$(".divHidden")[cont].id).removeClass("getUpList");    
           }
           catch{}            
       }
       iScrollPos_2=iCurScrollPos;
   });

   
   /*
                    Vector con todos los topes posibles en los scroll
   */
   var topes =[218,1280,1286,1990,2715,3070,3087,3432,4149,4866,5512,5563,5583,5597,6300,7017,7136,7187,7221,7734,8451,8777,8879,
               8913,9168,9885,10009,10452,10486,10602,11319,11582,12036,12042,12076,12753,13257,13465,13717,13768,13470,
               14187,14898,15000,15051,15260,15977,16539,16641,16689,16692,16694,17411,18128,18180,18316,18367,18845,
               19412,19565,19562,19616,20279,21070,21223,22319,22472,23977,24164,25618,25805,27310,27531,28525,28746,
               30132,30353,31756,31977,33346,33567,34970,35225,36202,36474,37877,38149,39807,39535,41108,41380,42527,42255];
   
    //Gradua la cantidad de pixeles capturados al hacer Scroll y retorna 1 en caso de llegar al fin del scroll
   function indiceGradual(val){
       
        salida=0;
        for(var i=0; i<topes.length;i++){                
            if( val==topes[i] ){
                salida=1;
                i=topes;
            }
            else if((val-topes[i])<2000 && (topes[i]-val)>2000){
                salida=1;
                i=topes;
            }
        }

        return salida;
   }
   
   

});