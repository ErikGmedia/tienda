<?php
session_start();

$user_id = (isset($_GET['user_id'])) ? $_GET['user_id'] : "";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0,minimun-scale=1.0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>GB MEDIA</title>

    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">

    <link rel="stylesheet" type="text/css" href="css/main.css">

    <link rel="stylesheet" type="text/css" href="css/carrito.css">
    
    <link type="text/css" rel="stylesheet" href="css/all_category.css" />
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="css/slick.css" />
    <!--<link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>-->

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="css/nouislider.min.css" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="css/style.css" />
    
    <link rel="icon" type="image/png" href="./img/elIco.ico">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
    <!--======================================================================================================-->
    <!--===============================================================================================-->

    <!--================================================================
 																PRELOADER
			====================================================================-->
    <!--
					PRELOADER CSS
				-->
    <link type="text/css" rel="stylesheet" href="css/loader.css" />
    <!--
				PRELOADER SCRIPTS
			-->
    <script src="js/pace.min.js"></script>
    <script src="js/modernizr.js"></script>
    
</head>

<body id="top">
    <input type="hidden" id="user_id" value="<?php echo str_replace('"',"",$user_id); ?>">

    <!-- HEADER -->
    <header>

        <!-- MAIN HEADER -->
        <div id="header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- LOGO -->
                    <div class="col-md-3">
                        <div class="header-logo">
                            <a href="index.php" class="logo">
                                <img src="./img/logoGBMediaGroup2.png" alt="Logo">
                            </a>
                        </div>
                    </div>
                    <!-- /LOGO -->

                    <!-- SEARCH BAR -->
                    <div class="col-md-6">

                    </div>
                    <!-- /SEARCH BAR -->

                    <!-- ACCOUNT -->
                    <div class="col-md-3 clearfix">
                        <div class="header-ctn">
                           
                            <!-- Cart -->
                            <div class="wrap-icon-header flex-w flex-r-m h-full">
                                <div class="flex-c-m h-full p-r-25 bor6">
                                    <div class="icon-header-item cl0 hov-cl1 trans-04 p-lr-11 icon-header-noti js-show-cart" id="detail_cart" data-notify="0"> 
                                        <i class='zmdi zmdi-shopping-cart' id="carrito"></i>
                                    </div>
                                </div>
                            </div>

                            
                            <div id="show_carrito" class="wrap-header-cart js-panel-cart ">
                                <div class="s-full js-hide-cart"></div>

                                <div class="header-cart flex-col-l p-l-65 p-r-25">
                                    <div class="header-cart-title flex-w flex-sb-m p-b-8">
                                        <span class="mtext-103 cl2">
														Carrito
													</span>

                                        <div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
                                            <i id='close_carrito' class="zmdi zmdi-close"></i>
                                        </div>
                                    </div>

                                    <div class="header-cart-content flex-w js-pscroll">
                                        <ul class="header-cart-wrapitem w-full barra_carrito" id="ul_car">

                                        </ul>
                                        <div class="w-full">
                                            <div class="header-cart-total w-full p-tb-40" id="total_cart">

                                            </div>

                                            <div class="header-cart-buttons flex-w w-full">
                                                <a href="./View/car.php" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
																Ver Carrito
															</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Cart -->

                            <!-- Menu Toogle -->
                            <div id="menuMobile" class="menu-toggle menuScrol">
                                <a href="#">
                                    <i class="fa fa-bars"></i>
                                    <span>Menu</span>
                                </a>
                            </div>
                            <!-- /Menu Toogle -->
                        </div>
                    </div>
                    <!-- /ACCOUNT -->
                </div>
                <!-- row -->
            </div>
            <!-- container -->
        </div>
        <!-- /MAIN HEADER -->
    </header>
    <!-- /HEADER -->
    <!-- NAVIGATION -->
    <nav id="navigation" >
        <!-- container -->
        <div class="container">
            <!-- responsive-nav -->
            <div id="responsive-nav">
                <!-- NAV -->
                <ul id="ulNav" class="main-nav nav navbar-nav" style="padding-top:3px;">
           
                </ul>
                <!-- /NAV -->
            </div>
            <!-- /responsive-nav -->
        </div>
        <!-- /container -->
    </nav>
    <!-- /NAVIGATION -->

    <!-- Slider -->
    <div id="carousel_1" class="carousel slide " data-ride="carousel" >
        <div class="carousel-inner " role="listbox">
            <div class="item active"id="slide1" >
                <img  src="img/BannerBody.jpg" alt="Item_1">
                <div class="carousel-caption hidden-xs hidden-sm">
                    <center><h1><button class="btnCarousel flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10 Scroller" >¡Comprar!</button></h1></center>
                </div>
            </div>
            <div class="item"  id="slide2" >
                <img src="img/BannerCosmetic.jpg" alt="Item_2">
                <div class="carousel-caption hidden-xs hidden-sm">
                    <center><h1><button class="btnCarousel flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10 Scroller" >¡Comprar!</button></h1></center>
                </div>
            </div>
            <div class="item"  id="slide3" >
                <img src="img/DSC09638.jpg" alt="Item_3">
                <div class="carousel-caption hidden-xs hidden-sm">
                    <center><h1><button class="btnCarousel flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10 Scroller" >¡Comprar!</button></h1></center>
                </div>
            </div>
            
            <!-- <div class="item"  id="slide4" >
                <img src="img/BannerLubri2.jpg" alt="Item_1">
                <div class="carousel-caption hidden-xs hidden-sm">
                    <center><h1><button class="btnCarousel flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10 Scroller" >¡Comprar!</button></h1></center>
                </div>
            </div> -->
        </div>
        <!-- CONTROLES-->
        <a href="#carousel_1" class="left carousel-control" role="button" data-slide="prev"><span id="spanLeft" class="glyphicon glyphicon-chevron-left" ></span></a>
        <a href="#carousel_1" class="right carousel-control" role="button" data-slide="next"><span id="spanRight" class="glyphicon glyphicon-chevron-right" ></span></a>
    </div>
            
    <!---SLIDER-->

    <!-- SECTION -->
    <div class="section" style="z-index: 9;">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">

                <!-- shop -->
                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                    <div class="shop">
                        <div class="shop-img" style="height: 380px;background: #fff;">
                            <img src="img/conso.png" alt="" style="width:142%;">
                        </div>
                        <div class="shop-body">
                            <h3>Dildos</h3>
                            <button onclick="setProductoCategoria('Dildos');" class="cta-btn Scroller">Ver Ahora <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </div>
                </div>
                <!-- /shop -->

                <!-- shop -->
                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                    <div class="shop" style="height: 380px;">
                        <div class="shop-img">
                            <img src="img/botella2.png" alt="" class="imgDownCarousel">
                        </div>
                        <div class="shop-body">
                            <h3>Lubricantes</h3>
                            <button onclick="setProductoCategoria('Lubricantes');" class="cta-btn Scroller" >Ver Ahora <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </div>
                </div>
                <!-- /shop -->

                <!-- shop -->
                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                    <div class="shop" style="height: 380px;">
                        <div class="shop-img">
                            <img src="img/0001-.jpg" alt="" >
                        </div>
                        <div class="shop-body">
                            <h3>Bodys</h3>
                            <button onclick="setProductoCategoria('Bodys');" class="cta-btn Scroller">Ver Ahora <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </div>
                </div>
                <!-- /shop -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->

    <!--MODAL QUICKVIEW-->
    <div id="modalQuick">

    </div>
    <!--MODAL-->

    <!-- SECTION -->
    <div >
        <!-- container -->
        <div  id="contProd" style="z-index: 0;">
            <!-- row -->
            <div class="row">

                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h3 class="title">Productos</h3>
                    </div>
                </div>
                <!-- /section title -->
                <!-- Products tab & slick -->
                <div class="col-md-12 " >
                    <div class="row pts_tarjet">
                        <div class="products-tabs">
                            <!-- tab -->
                            <div id="tab1" class="infinitScrol" data-nav="#slick-nav-1" >
                                   
                            </div>
                            <!-- /tab -->
                        </div>
                    </div>
                </div>
                <!-- Products tab & slick -->
                 <!-- Products tab & slick -->
                 <div class="col-md-12 " >
                    <div class="row pts_tarjet">
                        <div class="products-tabs">
                            <!-- tab -->
                            <div id="list_product" style="display:none;" class="infinitScrol" data-nav="#slick-nav-1" >
                                   
                            </div>
                            <!-- /tab -->
                        </div>
                    </div>
                </div>
                <!-- Products tab & slick -->
                
            </div>    
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->    


    <!--===========================================================================
												BEGIN	PRELOADER-LOADER
			==========================================================================--->

    <!-- preloader
    ================================================== -->
    <div id="preloader">
        <div id="loader">
            <div class="line-scale-pulse-out">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>

    <!--
			===============================================================================
												END	PRELOADER-LOADER
			===============================================================================
		-->

    <!--=================================================================================
																			QUICKVIEW
		=====================================================================================-->

    <div class="container">
        <div class="row">
            <div class="modal fade" tabindex="-1" role="dialog" id="modal_view">
                <div class="modal-dialog" style="width:60%;margin-top: 1%;">
                    <div class="modal-content">
                        <div class="modal-row">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modal_tittle"></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                            </div>
                            <div class="modal-body clearfix" id="modal_body">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal1 -->
	<div class="wrap-modal1 js-modal1 p-t-60 p-b-20">
		<div class="overlay-modal1 js-hide-modal1"></div>

		<div class="container">
			<div class="bg0 p-t-60 p-b-30 p-lr-15-lg how-pos3-parent">
				<button class="how-pos3 hov3 trans-04 js-hide-modal1">
                    <img src="img/icons/icon-close.png" alt="CLOSE">
                </button>

				<div class="row">
					<div class="col-md-6 col-lg-7 p-b-30">
						<div class="p-l-25 p-r-30 p-lr-0-lg">
							<div class="wrap-slick3 flex-sb flex-w">
								<div class="wrap-slick3-dots"></div>
								<div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

								<div class="slick3 gallery-lb">
									<div class="item-slick3" id="modal_img_thumb" data-thumb="">
										<div class="wrap-pic-w pos-relative">
											<img src="" id="modal_img_detail" alt="IMG-PRODUCT">

											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6 col-lg-5 p-b-30">
						<div class="p-r-50 p-t-5 p-lr-0-lg">
							<h1 class="mtext-105 cl2 js-name-detail p-b-14" id="modal_img_nombre">
								Lightweight Jacket
							</h1>

							$<span class="mtext-106 cl2" id="cart_price_">
								58.79
							</span>

							<!--  -->
							<div class="p-t-33">
								<div class="flex-w flex-r-m p-b-10">
									<div class="size-204 flex-w flex-m respon6-next" id="action-addCart">
										<div class="wrap-num-product flex-w m-r-20 m-tb-10">
											<div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-minus"></i>
											</div>

											<input class="mtext-104 cl3 txt-center num-product" id="cart_quantity_input_" type="number" name="num-product" value="1">

											<div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-plus"></i>
											</div>
										</div>

										<button type="button" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 js-addcart-detail">
											Agregar Al Carrito
										</button>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


    <!--=================================================================================
																			QUICKVIEW
		=====================================================================================-->
    <!-- FOOTER -->
    <footer id="footer" style="z-index: 1;">
        <!-- top footer -->
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-title">Gb MEDIA GROUP</h3>
                            <ul class="footer-links">
                                <p class="stext-107 cl7 size-201" >
                                    Somos un grupo empresarial joven y especializado en la creación y desarrollo de negocios online, obteniendo resultados que satisfacen sus necesidades.
                                </p>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3 col-xs-3">
                        <div class="footer col-md-1">
                            <h3 class="footer-title">Categorias</h3>
                            <ul class="footer-links">
                                <li><a class="Scroller" href="#" onclick="setProductoCategoria('Dildos');" >Dildos</a></li>
                                <li><a class="Scroller" href="#" onclick="setProductoCategoria('BabyDolls');" >BabyDolls</a></li>
                                <li><a class="Scroller" href="#" onclick="setProductoCategoria('Babuchas');" >Babuchas</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="clearfix visible-xs"></div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        <!-- /top footer -->

    </footer>
    <!-- /FOOTER -->

    <!-- jQuery Plugins -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/nouislider.min.js"></script>
    
    <script src="js/main.js"></script>
    <script src="js/loader.js"></script>
    <script src="js/gsdk-bootstrap-wizard.js"></script>
    <script src="js/jquery.bootstrap.wizard.js"></script>
    <script src="js/device-uuid.js"></script>

    <script src="js/cart.js"></script>
    <script src="js/sweetalert.min.js"></script>

    
    <!-- 
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script> -->
    <script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>

    <script>
        $(document).ready(function(){            
        //time for the carousel
            $("#carousel_1").carousel({
                   interval:2000
            }); 
        });
    </script>
    
    
    <script>
        function getQueryVariable(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i=0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if(pair[0] == variable) {
                    return pair[1];
                }
            }
            return false;
        }
        function Mensaje()
        {
            var m = getQueryVariable('m');
            if (m == "ok")
            {
                history.pushState(null, "", "index.php");
                swal({
                    title: "Enhorabuena",
                    text: "Nos estaremos comunicando con usted proximamente",
                    type: "success",
                    closeOnConfirm: true,
                    confirmButtonColor: "#2FDF50"
                });
            }

            if (m == "error")
            {
                swal({
                    title: "Lo sentimos",
                    text: "Hubo un error en el sistema, comuniquese con soporte",
                    type: "error",
                    closeOnConfirm: true,
                    confirmButtonColor: "#2FDF50"
                });
            }
        }
        window.onload = Mensaje();
        $(document).ready(function () {
            setCategorysMostSold();
            setAllProductos();

        });
    </script>
    
    
</body>

</html>